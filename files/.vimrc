" colorscheme molokai	" handy color scheme
syntax enable		" syntax highlighting

set expandtab		" insert spaces instead of tabs
set softtabstop=2	" spaces inserted when editing
set shiftwidth=2        " also 2 for beginning lines
set tabstop=8

set number		" show line numbers
set wildmenu		" show command completion
set lazyredraw          " only update the GUI when needed
set showmatch           " highlight matching [{()}]

set incsearch		" incremental search
set hlsearch		" highlight search matches

set laststatus=2        " always show the status bar
set ruler               " show row/column in status bar

set splitbelow          " open new splits below & right
set splitright

" wrap at word boundaries
set wrap
set linebreak
set showbreak=\ \ 

" look for .md files
set suffixesadd=.md

" make `gf` create a file if it doesn't exist
nmap gf :e <cfile><CR>

" add fzf mapping
if !empty(glob("/usr/local/opt/fzf"))
  set rtp+=/usr/local/opt/fzf
elseif !empty(glob("/home/linuxbrew/.linuxbrew/opt/fzf"))
  set rtp+=/home/linuxbrew/.linuxbrew/opt/fzf
endif

" set jk to escape, save all that movement
inoremap jk <esc>

" start of the day function
" optional argument is offset from today in days
function DayLog(...)
  let date = split(strftime("%Y %m %d"))
  let offset = "0"
  if a:0 > 0
    let offset = a:1
  endif
  let date_cmd = 'date -v +'.offset.'d "+%Y %m %d"'
  let date = split(system(date_cmd))
  let year  = date[0]
  let month = date[1]
  let day   = date[2]
  put = '### '.year.'-'.month.'-'.day
  put = ''
  put = year.'.'.month.'.'.day.': Meditate 0, Music 0, Social 0, Veggie 1, Slept 6, Spent 0, Alcohol 0, Cigarettes 0'
  put = ''
  put = '- breakfast:'
  put = '- lunch:'
  put = '- dinner:'
  put = ''
endfunction

function WeekLog()
  call DayLog(6)
  call DayLog(5)
  call DayLog(4)
  call DayLog(3)
  call DayLog(2)
  call DayLog(1)
  call DayLog(0)
endfunction

" save views when closing a file, load them on open.
augroup SaveManualFolds
  autocmd!
  au BufWinLeave ?* silent! mkview
  au BufWinEnter ?* silent! loadview
augroup END

" set ctrl+[hjkl] to navigate between splits
" note: ctrl+w <s> for horiz, <v> for vert.
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>

" set the open command to use the Mac built-in `open`
:let g:netrw_browsex_viewer= "open"

" nvim <Esc> to exit terminal-mode:
:tnoremap <Esc> <C-\><C-n>

" enable powerline fonts
let g:airline_powerline_fonts = 1

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" add a task estimate replacement
inoremap tke<Space> $e_{30}a_{x}$<Space>
inoremap tkee<Space> $e_{60}a_{x}$<Space>

" jour fixe and follow up shortcuts
inoremap jfw<Space> $e_{30}a_{x}$<Space>jour<Space>fixe<Space>with<Space>
inoremap fup<Space> **follow up**<Space>

" text replacements
inoremap bwn<Space> between<Space>
inoremap mtn<Space> meeting<Space>
inoremap pl<Space> people<Space>
inoremap schd<Space> schedule<Space>
