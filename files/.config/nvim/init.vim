set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc

" plugins
" 
" see https://vimawesome.com
"
" :PlugInstall (run after adding here)
" :PlugUpdate  (self-explanatory)
" :PlugClean   (remove unused)
call plug#begin()
Plug 'ctrlpvim/ctrlp.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'terryma/vim-multiple-cursors'
Plug 'koto-lang/koto.vim', {'branch': 'main'}
Plug 'raimondi/delimitmate'
Plug 'axvr/org.vim'
Plug 'ziglang/zig.vim'
" Plug 'tadaa/vimade'
" caused issues with opening with ctrl-p :(
call plug#end()

" ctrl+p key mapping
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

" airline theme
" let g:airline_theme='onedark'

