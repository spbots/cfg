# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE

# append to the history file, don't overwrite it
setopt APPEND_HISTORY

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTSIZE=1000
export HISTFILESIZE=2000

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
else
    export CLICOLOR
    alias ls='ls -G'
fi

. ~/.aliases
autoload -Uz compinit && compinit

export PATH="$HOME/.cargo/bin:$PATH"
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f /home/linuxbrew/.linuxbrew/bin/brew ] && \
    eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv) && \
    eval "$(nodenv init -)"
[ -f ~/.zshrc_ext ] && source ~/.zshrc_ext

echo
fortune | cowsay -n
echo
