;; set up straight.el and for package mgmt
;; https://github.com/radian-software/straight.el
;; use `straight-pull-all` to upgrade all packages
(setq straight-use-package-by-default t)
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; https://github.com/jwiegley/use-package
(straight-use-package 'use-package)

;; make backups and auto-save files in the temp directory instead of in the
;; directory where editing is happening.
(setq backup-directory-alist `(("." . ,temporary-file-directory)))
(setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

;; show line numbers in all buffers
;; http://ergoemacs.org/emacs/emacs_line_number_mode.html
;(global-display-line-numbers-mode)
;(setq display-line-numbers-type 'relative)

;; use word wrapping everywhere
(global-visual-line-mode 1)

;; highlight and auto-insert parens
(show-paren-mode 1)
(electric-pair-mode 1)

;; if in terminal, hide menu bar, tool bar
(if (not (display-graphic-p))
    (progn
      (tool-bar-mode -1)
      (menu-bar-mode -1)))

;; a command to kill any non-visible buffers
;; (useful for creating fast-loading perspectives)
;; https://emacs.stackexchange.com/a/12555
(defun sbs/kill-invisible-buffers ()
  "Kill all buffers not shown in a window somewhere."
  (interactive)
  (dolist (buf (buffer-list))
    (unless (get-buffer-window buf 'visible) (kill-buffer buf))))

;; a function for setting a collection of abbreviations that
;; are limited in scope to a buffer.
;; TODO needs some testing...
;; https://stackoverflow.com/a/49216125/101645
(defun sbs/set-local-abbrevs (abbrevs)
    "Add ABBREVS to `local-abbrev-table' and make it buffer local.
     ABBREVS should be a list of abbrevs as passed to `define-abbrev-table'.
     The `local-abbrev-table' will be replaced by a copy with the new
     abbrevs added, so that it is not the same as the abbrev table used
     in other buffers with the same `major-mode'."
    (let* ((bufname (buffer-name))
           (prefix (substring (md5 bufname) 0 (length bufname)))
           (tblsym (intern (concat prefix "-abbrev-table"))))
      (set tblsym (copy-abbrev-table local-abbrev-table))
      (dolist (abbrev abbrevs)
          (define-abbrev (eval tblsym)
            (car abbrev)
            (cadr abbrev)
            (caddr abbrev)))
    (setq-local local-abbrev-table (eval tblsym))))

;; pretty themes
(use-package one-themes)
(load-theme 'one-dark t)
(global-hl-line-mode 1)

;; integrate system clipboard
(use-package xclip)
(xclip-mode 1)

;; pretty status bar on the bottom
(use-package telephone-line)
(setq telephone-line-primary-right-separator 'telephone-line-abs-left
      telephone-line-secondary-right-separator 'telephone-line-abs-hollow-left)
(setq telephone-line-lhs
      '((evil   . (telephone-line-evil-tag-segment))
        (accent . (telephone-line-vc-segment
                   telephone-line-erc-modified-channels-segment
                   telephone-line-process-segment))
        (nil    . (telephone-line-buffer-segment))))
(setq telephone-line-rhs
      '((nil    . (telephone-line-misc-info-segment))
        (accent . (telephone-line-major-mode-segment))
        (evil   . (telephone-line-airline-position-segment))))
(telephone-line-mode 1)

;; try out magit. never exit emacs ever again.
(use-package magit)

;; use vim keybindings because C- and M- all the time is for masochists
(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-i-jump nil) ;; prevent evil from fucking up <TAB> in orgmode
                              ;; https://emacs.stackexchange.com/questions/69282
  :config
  (evil-mode 1)
  (evil-set-undo-system 'undo-redo))
(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))
(use-package evil-escape
  :init
  (setq evil-escape-key-sequence "jk")
  (setq evil-escape-excluded-major-modes '(magit-mode)) ;; https://github.com/emacs-evil/evil-magit/issues/4
  :config
  (evil-escape-mode 1))

;; helm for lots of autocomplete etc.
(use-package helm :config)
(helm-mode 1)

(setq helm-move-to-line-cycle-in-source t)        ; wrap helm lists:
(global-set-key (kbd "C-x C-f") `helm-find-files) ; helm-find-files is like ctrl+p
(global-set-key (kbd "M-x") `helm-M-x)            ; power up M-x
(global-set-key (kbd "M-y") 'helm-show-kill-ring) ; show the last yanked/"killed" stuff with M-y
(global-set-key (kbd "C-s") 'helm-occur)          ; replace emacs I-search with helm-occur

;; use perspective to remember window positions/sizes
;; prefix: C-x x
(use-package perspective
  :bind ("C-x C-b" . persp-list-buffers)
  :custom (persp-mode-prefix-key (kbd "C-x x"))
  :init (persp-mode))

;; which-key shows which options are available for an incomplete command
(use-package which-key)
(which-key-mode 1)

;; get dat orgmode
(use-package org)

;; see https://github.com/Somelauw/evil-org-mode/blob/master/doc/keythemes.org
;; for explanation of evil org key themes
(use-package evil-org
  :after (evil org)
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook
    (lambda ()
      (evil-org-set-key-theme '(insert
                                textobjects
                                additional
                                calendar))))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

;; Org mode configuration
(setq org-directory "~/Dropbox/Docs/notes")
(setq org-agenda-files '("~/Dropbox/Docs/notes"))
(setq org-footnote-section "footnotes")

;; prevent agenda from fucking up window splits, make the agenda
;; in the currently active window.
(setq org-agenda-window-setup 'current-window)

;; from https://orgmode.org/manual/Activation.html
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-c d") 'org-capture-at-point)
(define-key org-mode-map (kbd "C-c h") 'org-insert-heading)
(define-key org-mode-map (kbd "C-c ;") 'org-insert-structure-template)
(define-key org-mode-map (kbd "C-c i") 'org-id-get-create)
(setq org-startup-indented 1)
(setq org-startup-folded 'content)
(setq org-duration-format 'h:mm) ;; clock table sums in hours, not days
(push '(tags-tree . local) org-show-context-detail)

(setq org-todo-keywords
      '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCELED(c)" "INCOMPLETE(i)")))

(setq org-capture-templates '(
  ("d" "Date log" entry (file+olp+datetree "0.timelog.org")
   "\
* health\n\
- breakfast:\n\
- lunch:\n\
- dinner:\n\
* Band, Album :listen:music:\n\
* misc [/]\n\
* unplanned :unplanned:\n\
* daily summary :daily_summary:\n\
** successes\n\
** faltered\n\
** gratitude\n\
"
     :time-prompt t :tree-type week)
  ("s" "shopping item (not strictly necessary)" entry (file+headline "0.gtd.someday.org" "shopping")
     "* %t %^{what do you want?}"
     :immediate-finish t)
  ("u" "unplanned / distracting" entry (file "0.wip.org")
     "* %?")
  ("T" "test test test" entry (file+olp+datetree "test.org") "* test test test!" :time-prompt t :tree-type week)
))

;; enable languages for evaluation in org mode files
(org-babel-do-load-languages
 'org-babel-load-languages
 '((C . t)
   (emacs-lisp . t)
   (python . t)))

;; enable support for org -> markdown exporting
(require 'ox-md)

;; add markdown preview (does other stuff too)
(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("\\.md\\'" . markdown-mode))
  :init (setq markdown-command "pandoc"))

;; key substitutions, "abbrev mode"
(add-hook 'text-mode-hook #'abbrev-mode)

;; better c formatting
(setq c-default-style "k&r" c-basic-offset 4)

;; set custom personal dictionary (i.e. checked into git)
;; https://stackoverflow.com/questions/27544869/how-do-i-change-ispell-private-dictionary
(setq ispell-personal-dictionary "~/.emacs.d/.aspell.en.pws")

;; TEST TEST
;; https://stackoverflow.com/questions/4076360/error-in-dired-sorting-on-os-x
(when (eq system-type 'darwin)
  (require 'ls-lisp)
  (setq ls-lisp-use-insert-directory-program nil))

;; https://emacs.stackexchange.com/questions/74776/how-do-i-create-a-buffer-with-a-list-of-org-id-links-for-todo-items-across-all-m
(defun sbs/write-list-to-buffer ()
  (interactive)
;;  (let ((id-list (sbs/org-agenda-list-of-ids)))
  (let ((id-list org-agenda-files))
    (with-current-buffer (get-buffer-create "*List of IDs*")
      (delete-region (point-min) (point-max))
      (insert "holy moly!")
      (while id-list
        (insert (car id-list) "\n")
        (setq id-list (cdr id-list))))))

(defun sbs/org-agenda-list-of-ids()
  (let ((flist org-agenda-files)
        res)
    (while flist
      (with-current-buffer (find-file-noselect (car flist))
        (setq res (append res (do-something-with-buffer)))
        (setq flist (cdr flist))))
  res))

(defun do-something-with-buffer ()
  (org-element-map (org-element-parse-buffer)
                   'headline
                   #'do-something-with-headline))

(defun do-something-with-headline (el)
  (let ((id (org-element-property :ID el))
        (listed (org-element-property :LISTED el)))
    (when (and id (not listed))
      id)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("b73a23e836b3122637563ad37ae8c7533121c2ac2c8f7c87b381dd7322714cd0" "0dd2666921bd4c651c7f8a724b3416e95228a13fca1aa27dc0022f4e023bf197" default))
 '(fill-column 72)
 '(helm-minibuffer-history-key "M-p")
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
