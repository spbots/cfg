# cfg

config files for terminal apps etc.

## apps

when setting up, install

- dropbox
- neovim
- emacs
- git (if not already)
- fortune
- cowsay
- homebrew (linuxbrew)
  - fzf
  - nodenv
- rustup
- difftastic
- diodon (linux clipboard mgr)
