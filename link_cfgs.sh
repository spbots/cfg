#!/bin/sh

link_file_in_homedir()
{
  dname=`dirname "$1"`
  fname=`basename "$1"`
  targetname="$HOME/$1"

  # if the file already exists, and it's not a link, back it up
  [ -f "$targetname" ] && [ ! -L "$targetname" ] && mv "$targetname" "$targetname.spbak"

  # if there's not already a link, do it up
  [ ! -L "$targetname" ] && \
  mkdir -p $HOME/$dname && \
  ln -s "$PWD/$1" "$HOME/$dname/$fname"
}

rollback()
{
  targetname="$HOME/$1"
  [ -f "$targetname.spbak" ] && \
  rm "$targetname" && \
  mv "$targetname.spbak" "$targetname"
}

# link emacs.d if it doesn't exist
[ ! -d "$HOME/.emacs.d" ] && ln -s "$PWD/.emacs.d" "$HOME/.emacs.d"
[ -d "$HOME/.emacs.d" ] && [ ! -L "$HOME/.emacs.d" ] && \
  ( mv "$HOME/.emacs.d" "$HOME/.emacs.d.spbak" ; \
  ln -s "$PWD/.emacs.d" "$HOME/.emacs.d" )

cd "./files" && \
find . -type f | while read f;
  #do rollback "$f";
  do link_file_in_homedir "$f" && echo "[linked] $f" || echo "[exists] $f";
done;

